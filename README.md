RTR Configuration Management
============================

This repository contains
[Ansible](http://docs.ansible.com/ansible/index.html) "playbooks" for
managing the configuration of the Remote Tracking Range computers; both
the RTR nodes and the RICS system.

## Prerequisites

The control computer used to manage the configuration must be running
Linux or Unix (e.g. OS X). Windows cannot be used for control. Visit the
link above for Ansible installation instructions.

## Initial OS Installation

I have created a custom Debian Linux installation image for the RTR nodes,
it can be downloaded from the URL below:

https://drive.google.com/file/d/0BxlFz-T8y6CmWUhYSkdUVkM4Z0k/view?usp=sharing

This is a hybrid ISO image which can be copied directly to a USB thumb
drive. Instructions for doing so under Linux and Windows are available on
the [Debian web site](https://www.debian.org/CD/faq/#write-usb).

A standard Debian Linux installation can be done for the RICS system. Use
the latest release.

The initial Debian installation will configure the node to use DHCP to
obtain its IP address. Login as `root` via the console and run the command
below to read the IP address.

```
ip -f inet -o addr show eth0
```

### SSH Public Key

To avoid being prompted for a password, you should configure the `root`
and `rtr` accounts to allow you to log-in with a public key. The easiest
way to do this is by running the following command from the control
computer (after you have created a key-pair using `ssh-keygen`):

```
ssh-copy-id root@IPADDR
ssh-copy-id rtr@IPADDR
```

Replace *IPADDR* with the IP address of the RTR node.

## Inventory File

Create a file named `systems` in the repository directory and include the
following content:

```
[rtr]
HOSTNAME ansible_ssh_host=IPADDR
```

Where *HOSTNAME* is the hostname for the node (e.g. rtr-1) and *IPADDR* is
the DHCP IP address. To configure the RICS system, add a similar section
named `rics`:

```
[rics]
HOSTNAME ansible_ssh_host=IPADDR
```

### Example File

```
[rtr]
rtr-1 ansible_ssh_host=10.208.78.179
rtr-2 ansible_ssh_host=10.208.78.162
rtr-3 ansible_ssh_host=10.208.78.163
rtr-4 ansible_ssh_host=10.208.78.178
rtr-5 ansible_ssh_host=10.208.78.135
[rics]
rics-1 ansible_ssh_host=10.208.78.245
```

## Configuration

Run the following command from the top-level directory of the repository:

```shell
ansible-playbook -i systems provision.yaml
```

Consult the online Ansible documentation for further information.
