#!/bin/sh
### BEGIN INIT INFO
# Provides:          gpsport
# Required-Start:    $remote_fs $syslog $network
# Should-Start:      bluetooth dbus udev
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# X-Start-Before:    gpsd
# Short-Description: GPS serial port initialization
# Description:       This script initializes the baud rate of a serial port
#                    accessed by GPSd in order to bypass the daemon's auto
#                    baud feature.
### END INIT INFO
PATH=/sbin:/usr/sbin:/bin:/usr/bin
DESC="GPS serial port initialization"
NAME=gpsport

# Get device name of gpsd configuration
[ -r /etc/default/gpsd ] && . /etc/default/gpsd
# Get baud rate from our configuration
[ -r /etc/default/$NAME ] && . /etc/default/$NAME

# Load the VERBOSE setting and other rcS variables
. /lib/init/vars.sh

# Define LSB log_* functions.
# Depend on lsb-base (>= 3.0-6) to ensure that this file is present.
. /lib/lsb/init-functions


do_start() {
    [ "$VERBOSE" != no ] && log_daemon_msg "Running $NAME"
    for d in "$@"; do
        [ "$VERBOSE" != no ] && log_progress_msg " $d "
        stty speed $BAUD < $d
    done
    [ "$VERBOSE" != no ] && log_end_msg 0
}

do_stop() {
    return 0
}

case "$1" in
    start)
        do_start $DEVICES
        ;;
    stop)
        do_stop
        ;;
    *)
        echo "Usage: gpsport {start|stop}" >&2
        exit 3
        ;;
esac
